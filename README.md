# Proxy Cloudron App

This repository contains the Cloudron app package source for a plain proxy app.

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=proxy.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id proxy.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd proxy-app

cloudron build
cloudron install
```

## Usage

Use `cloudron push` to copy files into `/app/data/public/` and `cloudron exec` to get a remote terminal.

See https://cloudron.io/references/cli.html for how to get the `cloudron` command line tool.

