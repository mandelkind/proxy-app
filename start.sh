#!/bin/bash
set -eu
mkdir -p /app/data

# copy apache2 config if needed
if [[ ! -f "/app/data/apache2.conf" ]]; then
	cp /app/code/apache2.conf.template /app/data/apache2.conf
fi

## hook for custom start script in /app/data/run.sh
if [ -f "/app/data/run.sh" ]; then
    /bin/bash /app/data/run.sh
fi

chown -R www-data:www-data /app/data

echo "Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND