FROM cloudron/base:0.11.0

RUN mkdir -p /app/code
WORKDIR /app/code

RUN rm -rf /var/cache/apt /var/lib/apt/lists /etc/ssh_host_*

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
RUN echo "Listen 80" > /etc/apache2/ports.conf
RUN a2enmod ssl xml2enc proxy proxy_ajp proxy_http rewrite deflate headers proxy_balancer proxy_connect proxy_html

# configure site
COPY apache2.conf /app/code/apache2.conf.template
RUN ln -s /app/data/apache2.conf /etc/apache2/sites-enabled/app.conf

# add code
COPY start.sh /app/code/
RUN chmod +x /app/code/start.sh

# make cloudron exec sane
WORKDIR /app/data

CMD [ "/app/code/start.sh" ]